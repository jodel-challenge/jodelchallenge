package com.jodel.jodelchallenge.util

object Constant {
    val BASE_URL = "https://api.github.com/"
    val CONNECTION_TIMEOUT_SEC = 60L
    val READ_TIMEOUT_SEC = 60L
    val GIT_HUB_PERSONAL_TOKEN = "token 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"
}