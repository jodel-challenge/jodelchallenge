Jodel Coding Challenge
=============================

# App Design and language details:
* Language: Kotlin
* Dependency Injection: Koin
* Async Handling: Coroutine
* Navigation: Android Navigation Architecture
* Design Pattern: MVVM
* Reactive UI: LiveData, observables
* Network: Retrofit, GSON
* LocalDB: Realm DB

# UI

* ConstraintLayout
* RecyclerView
* CardView

# libraries:

* Coroutine - Aysnc Handling 
* Koin - DI
* Retrofit- Network Operations
* Gson - POJO Operation
* Glide - Image Loading
* Realm - Local DB

# APK
* https://bit.ly/3E2bZ7M


# Note
* Application is supporting offline functionality i.e showing results from previous search query.
* For GitHub search API Personal Access Token is used to increase the rate limit.